//
//  PinYinViewController.m
//  LanguageTest
//
//  Created by ZP on 14-8-2.
//  Copyright (c) 2014年 zp. All rights reserved.
//

#import "PinYinViewController.h"

@interface PinYinViewController ()
{
    UITextField *nameText;
    UILabel *name;
    UILabel *label;
    
}
@end

@implementation PinYinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //    NSString *str =@"周鹏";
    

    nameText =[[UITextField alloc]initWithFrame:CGRectMake(30, 140, 200, 50)];
    nameText.backgroundColor=[UIColor grayColor];
    nameText.placeholder=@"输入内容";
    nameText.returnKeyType=UIReturnKeyDone;
    nameText.textColor=[UIColor whiteColor];
    [self.view addSubview:nameText];
    
    
    
    UIButton *btn =[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(30, 90, 100, 40);
    [btn setTitle:@"转拼音" forState:(UIControlStateNormal)];
    btn.backgroundColor=[UIColor blueColor];
    btn.layer.cornerRadius=5;
    [btn.layer setMasksToBounds:YES];
    btn.selected=NO;
    [btn addTarget:self action:@selector(doBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:btn];
    
    label =[[UILabel alloc]initWithFrame:CGRectMake(30, 240, 200, 50)];
    label.backgroundColor=[UIColor grayColor];
    label.textColor=[UIColor whiteColor];
    [self.view addSubview:label];

}

- (void)doBtn:(UIButton *)sender
{
    sender.selected=!sender.selected;
    
    CFStringRef ref =(__bridge CFStringRef)nameText.text;
    CFMutableStringRef string =CFStringCreateMutableCopy(NULL, 0,ref);
    
    if(sender.selected)
    {
        CFStringTransform(string, NULL, kCFStringTransformToLatin, NO);
        label.text=(__bridge NSString *)string;
        
    }else
    {
        CFStringTransform(string, NULL, kCFStringTransformToLatin, YES);
        label.text=(__bridge NSString *)string;
    }
    
    [self.view endEditing:YES];
}

@end
