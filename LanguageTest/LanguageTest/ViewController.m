//
//  ViewController.m
//  LanguageTest
//
//  Created by ZP on 14-8-2.
//  Copyright (c) 2014年 zp. All rights reserved.
//

#import "ViewController.h"
#import "PinYinViewController.h"
#import "InterNationalViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

//    [[UILabel appearance] setBackgroundColor:[UIColor redColor]];
    NSArray *array =@[@"汉字-拼音转换",@"英汉转换"];
    for(int i=0;i<2;i++)
    {
        UIButton *pyBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        pyBtn.center=CGPointMake(self.view.center.x, 90+i*100);
        pyBtn.bounds=CGRectMake(0, 0, 150, 50);
        [pyBtn setTitle:[array objectAtIndex:i] forState:(UIControlStateNormal)];
        pyBtn.backgroundColor=[UIColor grayColor];
        pyBtn.layer.cornerRadius=5;
        pyBtn.layer.masksToBounds=YES;
        pyBtn.tag=i+10;
        [pyBtn addTarget:self action:@selector(doPyBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pyBtn];
    }
    
    
   
}
- (void)doPyBtn:(UIButton *)sender
{
    int index =sender.tag-10;
    switch (index) {
        case 0:
        {
            PinYinViewController *pyVC =[[PinYinViewController alloc]init];
            [self.navigationController pushViewController:pyVC animated:YES];
        }
            break;
            
        default:
        {
            InterNationalViewController *inter =[[InterNationalViewController alloc]init];
            [self.navigationController pushViewController:inter animated:YES];
        }
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
