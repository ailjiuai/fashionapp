//
//  InterNationalViewController.m
//  LanguageTest
//
//  Created by ZP on 14-8-2.
//  Copyright (c) 2014年 zp. All rights reserved.
//

#import "InterNationalViewController.h"
#import "InternationalControl.h"
@interface InterNationalViewController ()
{
    UILabel *label;
    UIButton *btn;
}
@end

@implementation InterNationalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(changeLanguage) name:@"changeLanguage" object:nil];
    [InternationalControl initUserLanguage];
    
    NSBundle *bundle =[InternationalControl bundle];
    
    NSString *inviteMsg =[bundle localizedStringForKey:@"key" value:nil table:@"Lanaguage"];
    NSString *buttonInfo =[bundle localizedStringForKey:@"buttonInfo" value:nil table:@"Lanaguage"];
    
    
    btn =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:self action:@selector(doBtn) forControlEvents:(UIControlEventTouchUpInside)];
    btn.center=CGPointMake(self.view.center.x, self.view.frame.size.height-70);
    [btn setTitle:buttonInfo forState:(UIControlStateNormal)];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor blueColor]];
    btn.layer.cornerRadius=5;
    btn.layer.masksToBounds=YES;
    btn.bounds=CGRectMake(0, 0, 200, 50);
    [self.view addSubview:btn];
    
    
    label =[[UILabel alloc]init];
    label.backgroundColor=[UIColor grayColor];
    label.textColor=[UIColor whiteColor];
    label.numberOfLines=0;
    label.center=CGPointMake(self.view.center.x, 90);
    label.bounds=CGRectMake(0, 0, 200, 50);
    label.text=inviteMsg;
    [self.view addSubview:label];
}
- (void)doBtn
{
    NSString *lan = [InternationalControl userLanguage];
    
    if([lan isEqualToString:@"en"]){//判断当前的语言，进行改变
        
        [InternationalControl setUserLanguage:@"zh-Hans"];
        
    }else{
        
        [InternationalControl setUserLanguage:@"en"];
    }
    
    //改变完成之后发送通知，告诉其他页面修改完成，提示刷新界面
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLanguage" object:nil];
}

- (void)changeLanguage
{
    [btn setTitle:[[InternationalControl bundle] localizedStringForKey:@"buttonInfo" value:nil table:@"Lanaguage"] forState:(UIControlStateNormal)];
    
    label.text =[[InternationalControl bundle] localizedStringForKey:@"key" value:nil table:@"Lanaguage"] ;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
