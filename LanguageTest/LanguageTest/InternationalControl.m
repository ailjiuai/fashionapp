//
//  InternationalControl.m
//  LanguageTest
//
//  Created by ZP on 14-8-2.
//  Copyright (c) 2014年 zp. All rights reserved.
//

#import "InternationalControl.h"

@implementation InternationalControl

static NSBundle *bundle=nil;
+ (NSBundle *)bundle
{
    return bundle;
}

+ (void)initUserLanguage
{
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    
    NSString *string =[def valueForKey:@"userLanguage"];
    
    if (string.length ==0)
    {
      //获取当前系统的语言版本
        NSArray *languages=[def objectForKey:@"AppleLanguages"];
        
        NSString *current =[languages objectAtIndex:0];
        
        string =current;
        
        [def setValue:current forKey:@"userLanguage"];
        
        [def synchronize];
    }
    
    NSString *path =[[NSBundle mainBundle] pathForResource:string ofType:@"1proj"];
    bundle = [NSBundle bundleWithPath:path];
}
+ (NSString *)userLanguage
{
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    NSString *language = [def valueForKey:@"userLanguage"];
    
    return language;
}

+ (void)setUserLanguage:(NSString *)language
{
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    
    NSString *path=[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
    
    //2.持久化
    [def setValue:language forKey:@"userLanguage"];
    
    [def synchronize];
}
@end
