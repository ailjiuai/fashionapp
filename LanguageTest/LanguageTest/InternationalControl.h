//
//  InternationalControl.h
//  LanguageTest
//
//  Created by ZP on 14-8-2.
//  Copyright (c) 2014年 zp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InternationalControl : NSObject

+ (NSBundle *)bundle;//获取当前资源文件

+ (void)initUserLanguage;//初始化语言文件


+ (NSString *)userLanguage;//获取应用当前语言

+ (void)setUserLanguage:(NSString *)language;//设置当前语言
@end
